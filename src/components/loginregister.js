import "./css/style.css"
function loginregister()
{
  return(
  <>
    <div className="login">
      <h1>BookMyShow</h1>
      <div className="links">
        <a href="{{ url_for('login') }}" className="active">Login</a>
        <a href="{{ url_for('register') }}">Register</a>
      </div>
      <form action="{{ url_for('login') }}" method="post">
        <label for="username"><i className="fas fa-user"></i></label>
        <input type="text" name="username" placeholder="Username" id="username" required />
        <label for="password"><i className="fas fa-lock"></i></label>
        <input type="password" name="password" placeholder="Password" id="password" required />
        <div className="msg"></div>
        <input type="submit" value="Login" />
      </form>
    </div>
  </>);}
  export default loginregister;