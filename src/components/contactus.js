import "./css/style6.css"
function contactus()
{
  return(
  <>
<main className="main">
  <h1>Contact form</h1>
  <div className="container">
    <section className="wrapper">
      <form className="form" onsubmit="return validateForm(this)">
        <div className="form-group">
          <label for="fullname" className="input-label">Full name</label>
          <input type="text" name="fullname" className="input-field" placeholder="Jane Doe"/>
          <span className="input-check">
            <i className="fas fa-check-circle"></i>
            <i className="fas fa-exclamation-circle"></i>
          </span>
          <small className="input-alert"></small>
        </div>
        <div className="form-group">
          <label for="email" className="input-label">Email</label>
          <input type="email" name="email" className="input-field" placeholder="janedoe@mail.com"/>
          <span className="input-check">
            <i className="fas fa-check-circle"></i>
            <i className="fas fa-exclamation-circle"></i>
          </span>
          <small className="input-alert"></small>
        </div>
        <div className="form-group">
          <label for="message" className="input-label">Message</label>
          <textarea name="message" className="input-field" rows="3" placeholder="Type Messages Here"></textarea>
          <span className="input-check">
            <i className="fas fa-check-circle"></i>
            <i className="fas fa-exclamation-circle"></i>
          </span>
          <small className="input-alert"></small>
        </div>
        <div className="form-group">
          <input type="submit" className="input-submit" value="Send Request"/>
        </div>
      </form>
    </section>
  </div>
</main>
</>);}
export default contactus;