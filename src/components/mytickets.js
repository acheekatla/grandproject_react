import "./css/style3.css"
function mytickets()
{
  return(
  <>
{/* <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
            <link rel="stylesheet" href="{{ url_for('static', filename='style3.css') }}"> */}

<div className="ticket">
	<div className="left">
		<div className="image">
			<img src = "https://www.thetelugufilmnagar.com/wp-content/uploads/2017/07/Nani-Interview-Stills-@-Ninnu-Kori_thetelugufilmnagar-42.jpg"/>
			<p className="admit-one">
				<span>ADMIT ONE</span>
				<span>ADMIT ONE</span>
				<span>ADMIT ONE</span>
			</p>
			<div className="ticket-number">
				<p>
					#20030220
				</p>
			</div>
		</div>
		<div className="ticket-info">
			<p className="date">
				<span>TUESDAY</span>
				<span className="june-29">August 29TH</span>
				<span>2022</span>
			</p>
			<div className="show-name">
				<h1>Night Roars</h1>
				<h2>Nani</h2>
			</div>
			<div className="time">
				<p>8:00 PM <span>TO</span> 11:00 PM</p>
				<p>DOORS <span>@</span> 7:00 PM</p>
			</div>
			<p className="location"><span>Prost</span>
				<span className="separator"><i className="far fa-smile"></i></span><span>Jubilee Hills, Hyderabad</span>
			</p>
		</div>
	</div>
	<div className="right">
		<p className="admit-one">
			<span>ADMIT ONE</span>
			<span>ADMIT ONE</span>
			<span>ADMIT ONE</span>
		</p>
		<div className="right-info-container">
			<div className="show-name">
				<h1>SOUR Prom</h1>
			</div>
			<div className="time">
				<p>8:00 PM <span>TO</span> 11:00 PM</p>
				<p>DOORS <span>@</span> 7:00 PM</p>
			</div>
			<div className="barcode">
				<img src="https://external-preview.redd.it/cg8k976AV52mDvDb5jDVJABPrSZ3tpi1aXhPjgcDTbw.png?auto=webp&s=1c205ba303c1fa0370b813ea83b9e1bddb7215eb" alt="QR code"/>
			</div>
			<p className="ticket-number">
				#20030220
			</p>
		</div>
	</div>
</div>
</>
);}
export default mytickets;