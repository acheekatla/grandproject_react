import "./css/style.css"
function bookticket()
{
  return(
  <>
{/* // <!DOCTYPE html>
// <html>
//   <head>
//     <meta charset="utf-8" />
//     <title>BOOKMYSHOW</title>
//     <link rel="stylesheet" href="{{ url_for('static', filename = 'style.css') }}" />
//     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" />
//   </head>
//   <body> */}
    <div className="register">
      <h1>BookMyShow</h1>
      <div className="links">
        <a href="{{ url_for('bookmyticket') }}" className="active">BOOKYOURTICKET</a>
      </div>
      <form action="{{ url_for('bookmyticket') }}" method="post" autocomplete="off">
        <label for="name"><i className="fas fa-user"></i></label>
        <input type="text" name="name" placeholder="name" id="name" required />
        <label for="moviename"><i className="fas fa-lock"></i></label>
        <input type="text" name="moviename" placeholder="moviename" id="moviename" required />
        <label for="moviehall"><i className="fas fa-envelope"></i></label>
        <input type="text" name="moviehall" placeholder="moviehall" id="moviehall" required />
        <div className="msg">{{ msg }}</div>
        <input type="submit" value="BOOK" />
      </form>
    </div>
  {/* </body>
</html> */}
</>);}
export default bookticket;