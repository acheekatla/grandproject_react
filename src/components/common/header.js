import "./css/style.css";
function header() {
  return (
    <>
      <div className="header">
        <Container style={{ height: "100%" }}>
          <div className="header__wrapper">
            <div className="header__left">
              <Logo />

              <AutoComplete
                options={options}
                className="search-input"
                onSelect={onSelect}
                onSearch={debounce(onSearch, 500)}
                placeholder="Search movie"
                allowClear={true}
                notFoundContent={
                  loading ? <Loading /> : options.length === 0 && "No data"
                }
              />
            </div>

            <div className="header__right">
              <div className="header__right__userInfo">
                {Object.keys(credential).length === 0 ? (
                  <Link to={ROUTES.LOGIN}>
                    <p className="header__right__userInfo--name">Login</p>
                  </Link>
                ) : (
                  <>
                    <img
                      src={credential.avatar}
                      alt="avatar"
                      className="header__right__userInfo--avatar"
                    />
                    <Dropdown
                      overlay={
                        <Menu>
                          <Menu.Item
                            key="0"
                            onClick={() =>
                              history.push(
                                `${ROUTES.PROFILE}/${credential?._id}`
                              )
                            }
                          >
                            Profile
                          </Menu.Item>
                          <Menu.Item key="1" onClick={() => handleLogout()}>
                            Logout
                          </Menu.Item>
                        </Menu>
                      }
                      trigger={["click"]}
                    >
                      <p
                        className="header__right__userInfo--name"
                        style={{ cursor: "pointer" }}
                      >
                        {credential?.username}
                      </p>
                    </Dropdown>
                  </>
                )}
              </div>
              <div className="header__right__location">
                <i
                  className="fa fa-map-marker-alt"
                  style={{ marginRight: 10 }}
                ></i>
                <span>Hyderabad</span>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </>
  );
}
export default header;